module.exports = {
    publicPath: './',
    devServer: {
        open: true,
        proxy: { //配置跨域
            '/didi-fnn': {
                // target: 'https://test.niuxf.com/didi-fnn/web/', //这里后台的地址模拟的;应该填写你们真实的后台接口
                target:'http://192.168.0.115:8080/didi-fnn/web/',
                ws: true,
                changOrigin: true, //允许跨域
                pathRewrite: {
                    '^/didi-fnn': ''
                }
            }

        }
    }
}