import Vue from 'vue'
import router from '@/router'
import axios from 'axios'

const http = axios.create({
    timeout: 1000 * 30,
    withCredentials: true,
    // baseURL: 'https://test.niuxf.com/didi-fnn/web',
    baseURL:'http://192.168.0.115:8080/didi-fnn/web',
    headers: {
        'Content-Type': 'application/json; charset=utf-8'
    }
})

/**
 * 请求拦截
 */
http.interceptors.request.use(config => {
    // 请求头带上token
    if (localStorage.getItem('token')) {
        config.headers.token = localStorage.getItem('token');
    }
    return config
}, error => {
    return Promise.reject(error)
})

/**
 * 响应拦截
 */
http.interceptors.response.use(response => {
    if (response.data && response.data.code === 401) { // 401, token失效
        router.push({ name: 'Login' })
    }
    return response
}, error => {
    return Promise.reject(error)
})


export default http