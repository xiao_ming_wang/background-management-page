import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
// 用户管路由
import Lawyer from '../views/user/lawyer.vue'
import Member from '../views/user/member.vue'
// 课程类列表路由 member/coursecategory
// 课程章节视频表 member/partvideo
// 课程表 member/course
// 课程标签表 member/courselabel
import CourseCategory from '../views/course/coursecategory.vue'
import Partvideo from '../views/course/partvideo.vue'
import Course from '../views/course/course.vue'
import Courselabel from '../views/course/courselabel.vue'
//banner图 member/banner
import Banner from '../views/Banner.vue'
// 云点播短视频
import short_video from '../views/short_video.vue'
// 富文本编辑器
import Editor from '../views/Editor.vue'
import bigEditor from '../views/course/EditorBig.vue'
Vue.use(VueRouter)
// { path: '/web/lawyer', name: 'Lawyer', component: Lawyer },
// { path: '/web/memberr', name: 'Membern', component: Member }
const routes = [

    { path: '/login', name: 'Login', component: Login },
    {
        path: '/',
        name: 'Home',
        component: Home,
        children: [
            { path: 'web/lawyer', name: 'Lawyer', component: Lawyer }, //律师表
            { path: 'web/member', name: 'Membern', component: Member }, //用户管理
            { path: 'member/coursecategory', name: 'CourseCategory', component: CourseCategory }, // 课程类列表路由
            { path: 'member/partvideo', name: 'Partvideo', component: Partvideo }, // 课程章节视频表
            { path: 'member/course', name: 'Course', component: Course }, // 课程表
            { path: 'member/courselabel', name: 'Courselabel', component: Courselabel }, // 课程标签表 
            { path: 'member/banner', name: 'Banner', component: Banner }, // 轮播图 
            { path: 'sys/editor', name: 'Editor', component: Editor }, // 富文本编辑器 
            { path: 'member/shortvideo', name: 'short_video', component: short_video },//云点播短视频
            { path: 'big/editor', name: 'big_editor', component: bigEditor },//文章资讯列表

        ]
    },

]

const router = new VueRouter({
    mode: 'hash',
    // base: process.env.BASE_URL,
    routes
})

//导航守卫，判断是否登录
router.beforeEach(async (to, from, next) => {
    if (to.path === '/login') {
        next()
    } else {
        const tokenStr = localStorage.getItem('token')
        if (!tokenStr) {
            next('/login')
        } else {
            next()
        }
    }
})

export default router